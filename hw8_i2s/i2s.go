package main

import (
	"fmt"
	"reflect"
)

func i2s(data interface{}, out interface{}) (err error) {
	dataValue := reflect.ValueOf(data)
	outValue := reflect.ValueOf(out)

	if outValue.Kind() != reflect.Ptr {
		return fmt.Errorf("struct type is not a pointer: %v", outValue.Kind())
	}
	outValue = outValue.Elem()

	err = i2sRecursive(&dataValue, &outValue)
	if err != nil {
		return
	}

	out = outValue.Interface()
	return
}

func i2sRecursive(data, out *reflect.Value) (err error) {
	switch data.Kind() {
	case reflect.Map:
		for _, key := range data.MapKeys() {
			mapValue := data.MapIndex(key)
			err = getValueRecursive(&mapValue)
			if err != nil {
				return
			}

			structValue := out.FieldByName(key.String())
			switch mapValue.Kind() {
			case reflect.Int, reflect.String, reflect.Bool:
				if structValue.Kind() != mapValue.Kind() {
					return fmt.Errorf("values do not match: interface value - %v, struct value - %v", mapValue.Kind(), structValue.Kind())
				}
				structValue.Set(mapValue)
			case reflect.Map:
				if structValue.Kind() != reflect.Struct {
					return fmt.Errorf("struct value type is not a struct: %v", structValue.Kind())
				}

				err = i2sRecursive(&mapValue, &structValue)
				if err != nil {
					return
				}
			case reflect.Slice:
				if structValue.Kind() != mapValue.Kind() {
					return fmt.Errorf("values do not match: interface value - %v, struct value - %v", mapValue.Kind(), structValue.Kind())
				}

				err = i2sRecursive(&mapValue, &structValue)
				if err != nil {
					return
				}
			}
		}
	case reflect.Slice:
		if out.Kind() != reflect.Slice {
			return fmt.Errorf("struct type is not a slice: %s", out.Type())
		}

		out.Set(reflect.MakeSlice(out.Type(), data.Len(), data.Cap()))
		for i := 0; i < data.Len(); i++ {
			dataIndex := data.Index(i)
			outIndex := out.Index(i)

			err = getValueRecursive(&dataIndex)
			if err != nil {
				return
			}

			err = i2sRecursive(&dataIndex, &outIndex)
			if err != nil {
				return
			}
		}
	}
	return
}

func getValueRecursive(value *reflect.Value) (err error) {
	switch value.Kind() {
	case reflect.Map, reflect.Struct, reflect.Slice, reflect.String, reflect.Bool:
		return
	case reflect.Float64:
		if value.Float() == float64(int(value.Float())) {
			valueInteger := int(value.Float())
			*value = reflect.New(reflect.TypeOf(valueInteger)).Elem()
			value.Set(reflect.ValueOf(valueInteger))
		} else {
			return fmt.Errorf("can not cast float64 value to int")
		}
	case reflect.Interface:
		*value = value.Elem()
		err = getValueRecursive(value)
		if err != nil {
			return
		}
	case reflect.Ptr:
		if value.IsValid() && !value.IsNil() {
			*value = value.Elem()
			err = getValueRecursive(value)
			if err != nil {
				return
			}
		} else {
			return
		}
	}
	return
}
