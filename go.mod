module golang-webservices-1

go 1.12

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/stretchr/testify v1.4.0
)
