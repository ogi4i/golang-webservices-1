package main

import "sync"

type (
	StatConsumers struct {
		mu        sync.RWMutex
		consumers []*StatConsumer
	}

	StatConsumer struct {
		mu       sync.RWMutex
		interval uint64
		stat     *Stat
	}
)

func (s *StatConsumer) incrementByConsumer(consumer string) {
	s.mu.RLock()
	_, ok := s.stat.ByConsumer[consumer]
	s.mu.RUnlock()

	s.mu.Lock()
	if !ok {
		s.stat.ByConsumer[consumer] = 1
	} else {
		s.stat.ByConsumer[consumer] += 1
	}
	s.mu.Unlock()
}

func (s *StatConsumer) incrementByMethod(method string) {
	s.mu.RLock()
	_, ok := s.stat.ByMethod[method]
	s.mu.RUnlock()

	s.mu.Lock()
	if !ok {
		s.stat.ByMethod[method] = 1
	} else {
		s.stat.ByMethod[method] += 1
	}
	s.mu.Unlock()
}

func (s *StatConsumer) reset() {
	s.mu.Lock()
	s.stat.ByMethod = make(map[string]uint64)
	s.stat.ByConsumer = make(map[string]uint64)
	s.mu.Unlock()
}
