package main

import "strings"

type (
	ACLs []*ACL

	ACL struct {
		consumer string
		allowed  []*method
	}

	method struct {
		Service string
		Name    string
	}
)

func (a ACLs) ACL(consumer string) *ACL {
	for _, acl := range a {
		if acl.consumer == consumer {
			return acl
		}
	}
	return nil
}

func (a *ACL) isAllowed(method string) bool {
	for _, service := range a.allowed {
		if service.Service != strings.Split(method, "/")[1] {
			continue
		}

		if service.Name == "*" || service.Name == strings.Split(method, "/")[2] {
			return true
		}
	}
	return false
}

func convertSliceToServiceInfo(s []string) []*method {
	services := make([]*method, 0, len(s))
	for _, str := range s {
		services = append(services, &method{
			Service: strings.Split(str, "/")[1],
			Name:    strings.Split(str, "/")[2],
		})
	}
	return services
}

func convertMapToACLs(m map[string][]string) ACLs {
	acls := ACLs{}
	for k, v := range m {
		acls = append(acls, &ACL{
			consumer: k,
			allowed:  convertSliceToServiceInfo(v),
		})
	}
	return acls
}
