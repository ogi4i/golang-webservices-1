package main

import (
	"context"
	"encoding/json"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/status"
	"log"
	"net"
	"sync"
	"time"
)

// тут вы пишете код
// обращаю ваше внимание - в этом задании запрещены глобальные переменные
type (
	Microservice struct {
		acls          ACLs
		logConsumers  *LogConsumers
		statConsumers *StatConsumers
	}

	LogConsumers struct {
		mu        sync.RWMutex
		consumers []chan *Event
	}
)

func NewMicroservice(acls ACLs) (*Microservice, error) {
	return &Microservice{
		acls: acls,
		logConsumers: &LogConsumers{
			mu:        sync.RWMutex{},
			consumers: make([]chan *Event, 0),
		},
		statConsumers: &StatConsumers{
			mu:        sync.RWMutex{},
			consumers: make([]*StatConsumer, 0),
		},
	}, nil
}

func (m *Microservice) hasAccess(ctx context.Context, method string) bool {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return false
	}

	for _, consumer := range md.Get("consumer") {
		if acl := m.acls.ACL(consumer); acl != nil {
			return acl.isAllowed(method)
		}
	}

	return false
}

func (m *Microservice) logRequest(ctx context.Context, method string) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return
	}

	var consumer string
	if s := md.Get("consumer"); s != nil {
		consumer = s[0]
	}

	p, ok := peer.FromContext(ctx)
	if !ok {
		return
	}

	event := &Event{
		Timestamp: time.Now().Unix(),
		Consumer:  consumer,
		Method:    method,
		Host:      p.Addr.String(),
	}

	m.logConsumers.mu.RLock()
	for _, ch := range m.logConsumers.consumers {
		ch <- event
	}
	m.logConsumers.mu.RUnlock()
}

func (m *Microservice) sendStat(ctx context.Context, method string) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return
	}

	var consumer string
	if s := md.Get("consumer"); s != nil {
		consumer = s[0]
	}

	m.statConsumers.mu.RLock()
	for _, c := range m.statConsumers.consumers {
		c.incrementByConsumer(consumer)
		c.incrementByMethod(method)
	}
	m.statConsumers.mu.RUnlock()
}

func (m *Microservice) Logging(_ *Nothing, stream Admin_LoggingServer) error {
	ch := make(chan *Event, 100)

	m.logConsumers.mu.Lock()
	m.logConsumers.consumers = append(m.logConsumers.consumers, ch)
	m.logConsumers.mu.Unlock()

	for e := range ch {
		stream.Send(e)
	}

	return nil
}

func (m *Microservice) Statistics(req *StatInterval, stream Admin_StatisticsServer) error {
	consumer := &StatConsumer{
		mu:       sync.RWMutex{},
		interval: req.IntervalSeconds,
		stat: &Stat{
			ByMethod:   make(map[string]uint64),
			ByConsumer: make(map[string]uint64),
		},
	}

	m.statConsumers.mu.Lock()
	m.statConsumers.consumers = append(m.statConsumers.consumers, consumer)
	m.statConsumers.mu.Unlock()

	for {
		select {
		case <-time.After(time.Duration(req.IntervalSeconds) * time.Second):
			consumer.mu.RLock()
			stream.Send(&Stat{
				Timestamp:  time.Now().Unix(),
				ByMethod:   consumer.stat.ByMethod,
				ByConsumer: consumer.stat.ByConsumer,
			})
			consumer.mu.RUnlock()

			consumer.reset()
		case <-stream.Context().Done():
			return nil
		}
	}
}

func (m *Microservice) Check(context.Context, *Nothing) (*Nothing, error) {
	return &Nothing{}, nil
}

func (m *Microservice) Add(context.Context, *Nothing) (*Nothing, error) {
	return &Nothing{}, nil
}

func (m *Microservice) Test(context.Context, *Nothing) (*Nothing, error) {
	return &Nothing{}, nil
}

func startGRPCServer(ctx context.Context, address string, acls ACLs) error {
	lis, err := net.Listen("tcp", address)
	if err != nil {
		return fmt.Errorf("failed to listen: %v", err)
	}

	m, err := NewMicroservice(acls)
	if err != nil {
		return fmt.Errorf("failed to create microservice instance: %v", err)
	}

	opts := []grpc.ServerOption{
		grpc.UnaryInterceptor(unaryInterceptor),
		grpc.StreamInterceptor(streamInterceptor),
	}
	grpcServer := grpc.NewServer(opts...)

	go func() {
		for {
			select {
			case <-ctx.Done():
				grpcServer.Stop()
				return
			}
		}
	}()

	RegisterAdminServer(grpcServer, m)
	RegisterBizServer(grpcServer, m)

	log.Printf("starting gRPC server on %s", address)
	if err := grpcServer.Serve(lis); err != nil {
		return fmt.Errorf("failed to serve: %s", err)
	}

	return nil
}

func StartMyMicroservice(ctx context.Context, address string, aclData string) error {
	m := make(map[string][]string, 0)
	err := json.Unmarshal([]byte(aclData), &m)
	if err != nil {
		return err
	}

	go func() {
		if err := startGRPCServer(ctx, address, convertMapToACLs(m)); err != nil {
			log.Fatalf("failed to start gRPC server: %s", err)
		}
	}()

	return nil
}

func unaryInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	m, ok := info.Server.(*Microservice)
	if !ok {
		return nil, fmt.Errorf("unable to cast server")
	}

	if !m.hasAccess(ctx, info.FullMethod) {
		return nil, status.Error(codes.Unauthenticated, "access not allowed for consumer")
	}

	m.logRequest(ctx, info.FullMethod)
	m.sendStat(ctx, info.FullMethod)

	return handler(ctx, req)
}

func streamInterceptor(srv interface{}, ss grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	m, ok := srv.(*Microservice)
	if !ok {
		return fmt.Errorf("unable to cast server")
	}

	if !m.hasAccess(ss.Context(), info.FullMethod) {
		return status.Error(codes.Unauthenticated, "access not allowed for consumer")
	}

	m.logRequest(ss.Context(), info.FullMethod)
	m.sendStat(ss.Context(), info.FullMethod)

	return handler(srv, ss)
}
