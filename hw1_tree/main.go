package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	out := os.Stdout

	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}

	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"

	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err)
	}
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	err := drawDirTree(out, path, printFiles, "")
	if err != nil {
		return err
	}

	return nil
}

func getFilesFromPath(path string, printFiles bool) ([]os.FileInfo, error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}

	if !printFiles {
		filteredFiles := make([]os.FileInfo, 0, len(files))
		for _, file := range files {
			if file.IsDir() {
				filteredFiles = append(filteredFiles, file)
			}
		}

		return filteredFiles, nil
	}

	return files, nil
}

func drawDirTree(out io.Writer, path string, printFiles bool, prefix string) error {
	const (
		VerticalLine   = "│"
		HorizontalLine = "─"
		TLikeLine      = "├"
		LLikeLine      = "└"
		Tab            = "\t"
		Newline        = "\n"
	)

	files, err := getFilesFromPath(path, printFiles)
	if err != nil {
		return err
	}

	for i, file := range files {
		out.Write([]byte(prefix))

		countFiles := len(files) - 1
		if i == countFiles {
			out.Write([]byte(LLikeLine))
		} else {
			out.Write([]byte(TLikeLine))
		}

		out.Write([]byte(strings.Repeat(HorizontalLine, 3)))

		if file.IsDir() {
			out.Write([]byte(file.Name() + Newline))

			subPath := path + string(os.PathSeparator) + file.Name()

			var subPrefix string
			if i == countFiles {
				subPrefix = prefix + Tab
			} else {
				subPrefix = prefix + VerticalLine + Tab
			}

			err := drawDirTree(out, subPath, printFiles, subPrefix)
			if err != nil {
				return err
			}
		} else {
			var fileSize string
			if file.Size() == 0 {
				fileSize = "(empty)"
			} else {
				fileSize = fmt.Sprintf("(%db)", file.Size())
			}

			out.Write([]byte(file.Name() + " " + fileSize + Newline))
		}
	}

	return nil
}
