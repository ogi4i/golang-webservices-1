package main

import (
	"encoding/json"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"os"
	"reflect"
	"strconv"
	"strings"
	"text/template"
)

// код писать тут
type (
	apiGenSpec struct {
		URL    string `json:"url"`
		Auth   bool   `json:"auth"`
		Method string `json:"method"`
	}

	apiSpec struct {
		Type         string
		VariableName string
		Methods      []*methodSpec
	}

	methodSpec struct {
		Name   string
		URL    string
		Auth   bool
		Method string
		Struct *structSpec
	}

	structSpec struct {
		Name   string
		Fields []*fieldSpec
	}

	fieldSpec struct {
		Name      string
		Type      string
		ParamName string
		Required  bool
		Min       *int
		Max       *int
		Enum      []interface{}
		Default   interface{}
	}
)

const (
	apiResponseTemplate = `
type ApiResponse struct {
	Response interface{} ` + "`" + `json:"response,omitempty"` + "`" + `
	Error string ` + "`" + `json:"error"` + "`" + `
}`

	httpErrorTemplate = `
func httpError(w http.ResponseWriter, code int, error string) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	body, _ := json.Marshal(&ApiResponse{Error: error})
	w.WriteHeader(code)
	w.Write(body)
}
`
)

var (
	subFuncMap = template.FuncMap{
		"sub": func(i, j int) int {
			return i - j
		},
	}

	structTemplate = template.Must(template.New("structTemplate").Parse(`type {{.Name}}Raw struct {
{{- range .Fields}}
	{{.Name}}  {{.Type}} ` + "`" + `json:"{{.ParamName}}"` + "`" + `{{end}}
}

`))

	serveHttpTemplate = template.Must(template.New("serveHttpTemplate").Parse(`func ({{.VariableName}} *{{.Type}}) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
{{- range .Methods}}
	case "{{.URL}}":
{{- if not .Method}}
		if r.Method == http.MethodGet || r.Method == http.MethodPost {
{{- else if eq .Method "POST"}}
		if r.Method == http.MethodPost {
{{- else if eq .Method "GET"}}
		if r.Method == http.MethodGet {
{{end}}
			srv.handle{{.Name}}(w, r)
		} else {
			httpError(w, http.StatusNotAcceptable, "bad method")
			return
		}
{{- end}}
	default:
		httpError(w, http.StatusNotFound, "unknown method")
		return
	}
}

`))

	handleMethodTemplate = template.Must(template.New("handleMethodTemplate").Funcs(subFuncMap).Parse(`{{- $api := .}}{{- range .Methods}}func ({{$api.VariableName}} *{{$api.Type}}) handle{{.Name}}(w http.ResponseWriter, r *http.Request) {
	var body []byte
	var err error
	resp := new(ApiResponse)
{{- if .Auth}}

	if r.Header.Get("X-Auth") != "100500" {
		httpError(w, http.StatusForbidden, "unauthorized")
		return
	}
{{end}}
	params := new({{.Struct.Name}}Raw)
{{- range .Struct.Fields}}
{{- if eq .Type "string"}}
	params.{{.Name}} = r.FormValue("{{.ParamName}}")
{{- else if eq .Type "int"}}

	{{.ParamName}}, err := strconv.Atoi(r.FormValue("{{.ParamName}}"))
	if err != nil {
		httpError(w, http.StatusBadRequest, "{{.ParamName}} must be int")
		return
	}
	params.{{.Name}} = {{.ParamName}}
{{end}}
{{- end}}
{{- range .Struct.Fields}}
{{- $field := .}}
{{- if .Required}}
{{- if eq .Type "string"}}
	if params.{{.Name}} == "" {
{{- else if eq .Type "int"}}
	if params.{{.Name}} == 0 {
{{- end}}
		httpError(w, http.StatusBadRequest, "{{.ParamName}} must me not empty")
		return
	}
{{- end}}
{{- if .Min}}
{{- if eq .Type "string"}}
	if len(params.{{.Name}}) < {{.Min}} {
		httpError(w, http.StatusBadRequest, "{{.ParamName}} len must be >= {{.Min}}")
		return
{{- else if eq .Type "int"}}
	if params.{{.Name}} < {{.Min}} {
		httpError(w, http.StatusBadRequest, "{{.ParamName}} must be >= {{.Min}}")
		return
{{- end}}
	}
{{- end}}
{{- if .Max}}
{{- if eq .Type "string"}}
	if len(params.{{.Name}}) > {{.Max}} {
		httpError(w, http.StatusBadRequest, "{{.ParamName}} len must be <= {{.Max}}")
		return
{{- else if eq .Type "int"}}
	if params.{{.Name}} > {{.Max}} {
		httpError(w, http.StatusBadRequest, "{{.ParamName}} must be <= {{.Max}}")
        return
{{- end}}
	}
{{- end}}
{{- if .Default}}
{{- if eq .Type "string"}}
	if params.{{.Name}} == "" {
		params.{{.Name}} = "{{.Default}}"
{{- else if eq .Type "int"}}
	if params.{{.Name}} == 0 {
		params.{{.Name}} = {{.Default}}
{{- end}}
	}
{{- end}}
{{- if .Enum}}
{{- if eq .Type "string"}}
	if {{range $i, $elem := .Enum}}params.{{$field.Name}} != "{{$elem}}"{{if ne $i (sub (len $field.Enum) 1)}} && {{end}}{{end}} {
{{- else if eq .Type "int"}}
	if {{range $i, $elem := .Enum}}params.{{$field.Name}} != {{$elem}}{{if ne $i (sub (len $field.Enum) 1)}} && {{end}}{{end}} {
{{- end}}
		httpError(w, http.StatusBadRequest, "{{.ParamName}} must be one of [{{range $i, $elem := .Enum}}{{$elem}}{{if ne $i (sub (len $field.Enum) 1)}}, {{end}}{{end}}]")
        return
	}
{{- end}}
{{end}}
	res, err := {{$api.VariableName}}.{{.Name}}(r.Context(), {{.Struct.Name}}{
{{- range .Struct.Fields}}
		{{.Name}}:  params.{{.Name}},
{{- end}}
	})

	if err != nil {
		apiErr, ok := err.(ApiError)

		if ok {
			httpError(w, apiErr.HTTPStatus, apiErr.Error())
        	return
		} else {
			httpError(w, http.StatusInternalServerError, err.Error())
        	return
		}
	}

	resp.Response = res

	body, err = json.Marshal(resp)
	if err != nil {
		httpError(w, http.StatusInternalServerError, err.Error())
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(body)
}

{{end}}`))
)

func main() {
	fset := token.NewFileSet()
	node, err := parser.ParseFile(fset, os.Args[1], nil, parser.ParseComments)
	if err != nil {
		log.Fatal(err)
	}

	templateData := make(map[string]*apiSpec, 0)

	for _, decl := range node.Decls {
		switch decl.(type) {
		case *ast.FuncDecl:
			funcDecl := decl.(*ast.FuncDecl)

			if funcDecl.Doc == nil {
				fmt.Printf("SKIP function %#v does not have comments\n", funcDecl.Name.Name)
				continue
			}

			apiGen := new(apiGenSpec)
			var hasApiGenComment bool
			for _, comment := range funcDecl.Doc.List {
				if !strings.HasPrefix(comment.Text, "// apigen:api") {
					continue
				}

				apiGenStr := strings.TrimPrefix(comment.Text, "// apigen:api ")

				err := json.NewDecoder(strings.NewReader(apiGenStr)).Decode(apiGen)
				if err != nil {
					fmt.Printf("SKIP function %#v could not decode apigen mark\n", funcDecl.Name.Name)
					continue
				}

				hasApiGenComment = true
				break
			}

			if !hasApiGenComment {
				fmt.Printf("SKIP function %#v does not have apigen mark\n", funcDecl.Name.Name)
				continue
			}

			fmt.Printf("process function %s\n", funcDecl.Name.Name)

			if funcDecl.Recv.NumFields() != 1 {
				fmt.Printf("SKIP function %#v does not have a single reciever\n", funcDecl.Name.Name)
				continue
			}

			variableName := funcDecl.Recv.List[0].Names[0].Name
			apiName := funcDecl.Recv.List[0].Type.(*ast.StarExpr).X.(*ast.Ident).Name

			apiMethod := &methodSpec{
				Name:   funcDecl.Name.Name,
				URL:    apiGen.URL,
				Auth:   apiGen.Auth,
				Method: apiGen.Method,
				Struct: nil,
			}

			paramStruct := new(structSpec)
			for _, param := range funcDecl.Type.Params.List {
				if paramType, ok := param.Type.(*ast.Ident); ok {
					if paramType.Obj.Kind != ast.Typ {
						continue
					}

					fieldList := paramType.Obj.Decl.(*ast.TypeSpec).Type.(*ast.StructType).Fields.List

					paramStruct.Name = paramType.Name
					paramStruct.Fields = make([]*fieldSpec, 0, len(fieldList))

					for _, field := range fieldList {
						fieldName := field.Names[0].Name
						fieldType := field.Type.(*ast.Ident).Name

						fieldInfo := &fieldSpec{
							Name:      fieldName,
							Type:      fieldType,
							ParamName: strings.ToLower(fieldName),
							Required:  false,
							Min:       nil,
							Max:       nil,
							Enum:      nil,
							Default:   nil,
						}

						tag := reflect.StructTag(field.Tag.Value[1 : len(field.Tag.Value)-1])
						fieldTagStr := tag.Get("apivalidator")
						if fieldTagStr != "" {
							fieldTags := strings.Split(fieldTagStr, ",")

							for _, elem := range fieldTags {
								kvTags := strings.Split(elem, "=")

								switch kvTags[0] {
								case "required":
									fieldInfo.Required = true
								case "paramname":
									fieldInfo.ParamName = kvTags[1]
								case "enum":
									values := strings.Split(kvTags[1], "|")
									fieldInfo.Enum = make([]interface{}, 0, len(values))

									for _, s := range values {
										switch fieldType {
										case "string":
											fieldInfo.Enum = append(fieldInfo.Enum, s)
										case "int":
											i, err := strconv.Atoi(s)
											if err != nil {
												fmt.Printf("SKIP field %#v enum values must be int\n", field.Names[0].Name)
												continue
											}
											fieldInfo.Enum = append(fieldInfo.Enum, i)
										}
									}
								case "default":
									switch fieldType {
									case "string":
										fieldInfo.Default = kvTags[1]
									case "int":
										i, err := strconv.Atoi(kvTags[1])
										if err != nil {
											fmt.Printf("SKIP field %#v default value must be int\n", field.Names[0].Name)
											continue
										}
										fieldInfo.Default = i
									}
								case "min":
									i, err := strconv.Atoi(kvTags[1])
									if err != nil {
										fmt.Printf("SKIP field %#v min value must be int\n", field.Names[0].Name)
										continue
									}
									fieldInfo.Min = &i
								case "max":
									i, err := strconv.Atoi(kvTags[1])
									if err != nil {
										fmt.Printf("SKIP field %#v max value must be int\n", field.Names[0].Name)
										continue
									}
									fieldInfo.Max = &i
								}
							}
						}

						paramStruct.Fields = append(paramStruct.Fields, fieldInfo)
					}
				}
			}

			apiMethod.Struct = paramStruct

			if _, ok := templateData[apiName]; ok {
				templateData[apiName].Methods = append(templateData[apiName].Methods, apiMethod)
			} else {
				templateData[apiName] = &apiSpec{
					Type:         apiName,
					VariableName: variableName,
					Methods:      []*methodSpec{apiMethod},
				}
			}
		default:
			fmt.Printf("SKIP %T is not *ast.FuncDecl\n", decl)
			continue
		}
	}

	out, _ := os.Create(os.Args[2])
	defer out.Close()

	fmt.Fprintln(out, `package `+node.Name.Name)
	fmt.Fprintln(out) // empty line
	fmt.Fprintln(out, `import "encoding/json"`)
	fmt.Fprintln(out, `import "net/http"`)

apiLoop:
	for _, api := range templateData {
		for _, method := range api.Methods {
			for _, field := range method.Struct.Fields {
				if field.Type == "int" {
					fmt.Fprintln(out, `import "strconv"`)
					break apiLoop
				}
			}
		}
	}

	fmt.Fprintln(out) // empty line

	fmt.Fprintln(out, apiResponseTemplate)
	fmt.Fprintln(out, httpErrorTemplate)
	for _, api := range templateData {
		for _, method := range api.Methods {
			structTemplate.Execute(out, method.Struct)
		}

		serveHttpTemplate.Execute(out, api)

		handleMethodTemplate.Execute(out, api)
	}
}

// go build handlers_gen/* && ./codegen api.go api_handlers.go
// go test -v
