package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"strings"
)

type column struct {
	Name            string
	Type            string
	Length          int
	Nullable        bool
	PrimaryKey      bool
	Default         interface{}
	IsAutoIncrement bool
}

func (c *column) isValidData(data interface{}, query query) bool {
	if query == updateQuery && c.PrimaryKey {
		return false
	}

	if data == nil && (c.Nullable || c.Default == nil) {
		return true
	}

	switch data.(type) {
	case json.Number:
		_, err := data.(json.Number).Int64()
		if err != nil {
			_, err := data.(json.Number).Float64()
			if err != nil {
				return false
			}

			if c.Type == "float" || c.Type == "double" {
				return true
			}
		}

		if c.Type == "int" || c.Type == "smallint" || c.Type == "bigint" {
			return true
		}
	case string:
		if c.Type == "char" || c.Type == "varchar" || c.Type == "text" {
			if c.Length != 0 {
				if len(data.(string)) <= c.Length {
					return true
				}
			}

			return true
		}
	case bool:
		if c.Type == "bool" {
			return true
		}
	}

	return false
}

type columns []*column

func (cc columns) GetPrimaryKey() string {
	for _, c := range cc {
		if c.PrimaryKey {
			return c.Name
		}
	}
	return ""
}

func (cc columns) PlaceholdersFromMap(data map[string]interface{}) string {
	placeholders := make([]string, 0, len(cc))
	for _, c := range cc {
		if _, ok := data[c.Name]; ok {
			placeholders = append(placeholders, fmt.Sprintf("%s = ? ", c.Name))
		}
	}
	return strings.Join(placeholders, ",")
}

func (cc columns) Values() []interface{} {
	vals := make([]interface{}, 0, len(cc))
	for _, c := range cc {
		switch c.Type {
		case "int", "smallint", "bigint":
			vals = append(vals, new(sql.NullInt64))
		case "float", "double":
			vals = append(vals, new(sql.NullFloat64))
		case "bool":
			vals = append(vals, new(sql.NullBool))
		case "char", "varchar", "text":
			vals = append(vals, new(sql.NullString))
		}
	}
	return vals
}

func (cc columns) ValuesFromMap(data map[string]interface{}, query query) ([]interface{}, error) {
	result := make([]interface{}, 0, len(cc))
	for _, c := range cc {
		if v, ok := data[c.Name]; ok {
			if !c.isValidData(v, query) {
				return nil, fmt.Errorf("field %s have invalid type", c.Name)
			}

			if c.PrimaryKey && c.IsAutoIncrement {
				result = append(result, new(sql.NullInt64))
				continue
			}

			result = append(result, v)
			continue
		}

		if query == insertQuery {
			switch c.Type {
			case "smallint", "int", "bigint":
				if c.Nullable || c.Default == nil {
					result = append(result, new(sql.NullInt64))
				} else {
					result = append(result, &sql.NullInt64{Valid: true, Int64: c.Default.(int64)})
				}
			case "char", "varchar", "text":
				if c.Nullable || c.Default == nil {
					result = append(result, new(sql.NullString))
				} else {
					result = append(result, &sql.NullString{Valid: true, String: c.Default.(string)})
				}
			case "float", "double":
				if c.Nullable || c.Default == nil {
					result = append(result, new(sql.NullFloat64))
				} else {
					result = append(result, &sql.NullFloat64{Valid: true, Float64: c.Default.(float64)})
				}
			case "bool":
				if c.Nullable || c.Default == nil {
					result = append(result, new(sql.NullBool))
				} else {
					result = append(result, &sql.NullBool{Valid: true, Bool: c.Default.(bool)})
				}
			}
		}
	}

	return result, nil
}

func (cc columns) ValuesToMap(data []interface{}) map[string]interface{} {
	result := make(map[string]interface{})
	for i, value := range data {
		columnName := cc[i].Name

		switch value.(type) {
		case *sql.NullString:
			if value.(*sql.NullString).Valid {
				result[columnName] = value.(*sql.NullString).String
			} else {
				result[columnName] = nil
			}
		case *sql.NullInt64:
			if value.(*sql.NullInt64).Valid {
				result[columnName] = value.(*sql.NullInt64).Int64
			} else {
				result[columnName] = nil
			}
		case *sql.NullFloat64:
			if value.(*sql.NullFloat64).Valid {
				result[columnName] = value.(*sql.NullFloat64).Float64
			} else {
				result[columnName] = nil
			}
		case *sql.NullBool:
			if value.(*sql.NullBool).Valid {
				result[columnName] = value.(*sql.NullBool).Bool
			} else {
				result[columnName] = nil
			}
		default:
			result[columnName] = value
		}
	}
	return result
}
