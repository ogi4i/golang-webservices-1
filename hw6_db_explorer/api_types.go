package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type apiResponse struct {
	Response map[string]interface{} `json:"response,omitempty"`
	Error    string                 `json:"error,omitempty"`
}

func (a *apiResponse) String() string {
	bodyBytes, _ := json.Marshal(a)
	return string(bodyBytes)
}

type apiError struct {
	code int
	err  string
}

func (ae *apiError) Error() string {
	return ae.err
}

func httpError(w http.ResponseWriter, error string, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	fmt.Fprintln(w, &apiResponse{Error: error})
}
