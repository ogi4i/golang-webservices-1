package main

type table struct {
	name    string
	columns columns
}

func (t *table) Columns() columns {
	return t.columns
}

type tables []*table

func (tt tables) Table(table string) *table {
	for _, t := range tt {
		if table == t.name {
			return t
		}
	}
	return nil
}

func (tt tables) Exists(table string) bool {
	for _, t := range tt {
		if table == t.name {
			return true
		}
	}
	return false
}
