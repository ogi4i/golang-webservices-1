package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strconv"
	"strings"

	"github.com/go-sql-driver/mysql"
)

// тут вы пишете код
// обращаю ваше внимание - в этом задании запрещены глобальные переменные
type (
	query string

	DbExplorer struct {
		db     *sql.DB
		tables tables
	}
)

const (
	selectQuery query = "SELECT"
	insertQuery query = "INSERT"
	updateQuery query = "UPDATE"
	deleteQuery query = "DELETE"
)

func getTables(db *sql.DB) ([]string, error) {
	rows, err := db.Query("SHOW TABLES")
	if err != nil {
		return nil, err
	}

	cols, err := rows.Columns()
	if err != nil {
		return nil, err
	}

	vals := make([]interface{}, len(cols))
	for i := range cols {
		vals[i] = new(string)
	}

	res := make([]string, 0)
	for rows.Next() {
		err = rows.Scan(vals...)
		if err != nil {
			return nil, err
		}

		switch vals[0].(type) {
		case *string:
			res = append(res, *vals[0].(*string))
		}
	}

	return res, nil
}

func getColumnsFromTable(db *sql.DB, table string) ([]*column, error) {
	rows, err := db.Query(fmt.Sprintf("SHOW FULL COLUMNS FROM %s", table))
	if err != nil {
		return nil, err
	}

	cols, err := rows.Columns()
	if err != nil {
		return nil, err
	}

	vals := make([]interface{}, 0, len(cols))
	for range cols {
		vals = append(vals, new(sql.RawBytes))
	}

	res := make([]*column, 0)
	for rows.Next() {
		err = rows.Scan(vals...)
		if err != nil {
			return nil, err
		}

		column := new(column)
		for i, c := range cols {
			value := string(*vals[i].(*sql.RawBytes))

			switch c {
			case "Field":
				column.Name = value
			case "Type":
				re := regexp.MustCompile(`^(\w+)(\((\d+)\))?$`)
				if !re.MatchString(value) {
					return nil, fmt.Errorf("failed to parse Type: %s", value)
				}

				match := re.FindStringSubmatch(value)
				column.Type = match[1]
				column.Length, _ = strconv.Atoi(match[3])
			case "Null":
				if value == "YES" {
					column.Nullable = true
				}
			case "Key":
				if value == "PRI" {
					column.PrimaryKey = true
				}
			case "Default":
				if value == "NULL" {
					column.Default = nil
				} else {
					column.Default = value
				}
			case "Extra":
				if value == "auto_increment" {
					column.IsAutoIncrement = true
				}
			}
		}

		res = append(res, column)
	}

	return res, nil
}

func NewDbExplorer(db *sql.DB) (*DbExplorer, error) {
	tables, err := getTables(db)
	if err != nil {
		return nil, err
	}

	tt := make([]*table, 0, len(tables))
	for _, t := range tables {
		table := new(table)
		columns, err := getColumnsFromTable(db, t)
		if err != nil {
			return nil, err
		}

		table.name = t
		table.columns = columns

		tt = append(tt, table)
	}

	return &DbExplorer{
		db:     db,
		tables: tt,
	}, nil
}

func (dbe *DbExplorer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var head string
	var tail string
	head, tail = shiftPath(r.URL.Path)

	switch head {
	case "":
		switch r.Method {
		case http.MethodGet:
			dbe.handleGetTables(w, r)
		default:
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}
	default:
		if !dbe.tables.Exists(head) {
			httpError(w, "unknown table", http.StatusNotFound)
			return
		}

		table := head
		head, tail = shiftPath(tail)

		switch head {
		case "":
			switch r.Method {
			case http.MethodGet:
				var limit int
				var offset int
				limit = 5

				if value := r.FormValue("offset"); value != "" {
					if v, err := strconv.Atoi(value); err == nil {
						offset = v
					}
				}

				if value := r.FormValue("limit"); value != "" {
					if v, err := strconv.Atoi(value); err == nil {
						limit = v
					}
				}

				dbe.handleGetTableRows(w, r, table, offset, limit)
			case http.MethodPut:
				dbe.handleCreateTableRow(w, r, table)
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
				return
			}
		default:
			if tail != "/" {
				w.WriteHeader(http.StatusNotFound)
				return
			}

			id, err := strconv.Atoi(head)
			if err != nil {
				http.Error(w, fmt.Sprintf("invalid id: %q", head), http.StatusBadRequest)
				return
			}

			switch r.Method {
			case http.MethodGet:
				dbe.handleGetRowByID(w, r, table, id)
			case http.MethodPost:
				dbe.handleUpdateTableRowByID(w, r, table, id)
			case http.MethodDelete:
				dbe.handleDeleteTableRowByID(w, r, table, id)
			default:
				w.WriteHeader(http.StatusMethodNotAllowed)
				return
			}
		}
	}
}

func (dbe *DbExplorer) handleGetTables(w http.ResponseWriter, r *http.Request) {
	resp := new(apiResponse)
	res := make([]string, 0, len(dbe.tables))
	for _, t := range dbe.tables {
		res = append(res, t.name)
	}

	resp.Response = map[string]interface{}{
		"tables": res,
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(resp.String()))
}

func (dbe *DbExplorer) handleGetTableRows(w http.ResponseWriter, r *http.Request, table string, offset int, limit int) {
	resp := new(apiResponse)

	res, err := dbe.getTableRows(r.Context(), table, offset, limit)
	if err != nil {
		httpError(w, "internal server error", http.StatusInternalServerError)
		return
	}

	resp.Response = map[string]interface{}{
		"records": res,
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(resp.String()))
}

func (dbe *DbExplorer) handleGetRowByID(w http.ResponseWriter, r *http.Request, table string, id int) {
	resp := new(apiResponse)

	res, err := dbe.getRowByID(r.Context(), table, id)
	if err != nil {
		if v, ok := err.(*apiError); ok {
			httpError(w, v.Error(), v.code)
			return
		}

		httpError(w, "internal server error", http.StatusInternalServerError)
		return
	}

	resp.Response = map[string]interface{}{
		"record": res,
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(resp.String()))
}

func (dbe *DbExplorer) handleCreateTableRow(w http.ResponseWriter, r *http.Request, table string) {
	resp := new(apiResponse)

	data := make(map[string]interface{})
	decoder := json.NewDecoder(r.Body)
	decoder.UseNumber()
	err := decoder.Decode(&data)
	if err != nil {
		httpError(w, "internal server error", http.StatusInternalServerError)
		return
	}

	res, err := dbe.createTableRow(r.Context(), table, data)
	if err != nil {
		if v, ok := err.(*apiError); ok {
			httpError(w, v.Error(), v.code)
			return
		}

		httpError(w, "internal server error", http.StatusInternalServerError)
		return
	}

	resp.Response = res
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(resp.String()))
}

func (dbe *DbExplorer) handleUpdateTableRowByID(w http.ResponseWriter, r *http.Request, table string, id int) {
	resp := new(apiResponse)

	data := make(map[string]interface{})
	decoder := json.NewDecoder(r.Body)
	decoder.UseNumber()
	err := decoder.Decode(&data)
	if err != nil {
		httpError(w, "internal server error", http.StatusInternalServerError)
		return
	}

	affectedID, err := dbe.updateTableRowByID(r.Context(), table, id, data)
	if err != nil {
		if v, ok := err.(*apiError); ok {
			httpError(w, v.Error(), v.code)
			return
		}

		httpError(w, "internal server error", http.StatusInternalServerError)
		return
	}

	resp.Response = map[string]interface{}{
		"updated": affectedID,
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(resp.String()))
}

func (dbe *DbExplorer) handleDeleteTableRowByID(w http.ResponseWriter, r *http.Request, table string, id int) {
	resp := new(apiResponse)

	affectedID, err := dbe.deleteRowByID(r.Context(), table, id)
	if err != nil {
		if v, ok := err.(*apiError); ok {
			httpError(w, v.Error(), v.code)
			return
		}

		httpError(w, "internal server error", http.StatusInternalServerError)
		return
	}

	resp.Response = map[string]interface{}{
		"deleted": affectedID,
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(resp.String()))
}

func (dbe *DbExplorer) getTableRows(ctx context.Context, table string, offset int, limit int) ([]map[string]interface{}, error) {
	rows, err := dbe.db.QueryContext(ctx, fmt.Sprintf("SELECT * FROM %s LIMIT ?, ?", table), offset, limit)
	if err != nil {
		return nil, err
	}

	columns := dbe.tables.Table(table).Columns()
	values := columns.Values()

	res := make([]map[string]interface{}, 0)
	for rows.Next() {
		err = rows.Scan(values...)
		if err != nil {
			return nil, err
		}

		res = append(res, columns.ValuesToMap(values))
	}

	if len(res) == 0 {
		fmt.Println(res)
	}

	return res, nil
}

func (dbe *DbExplorer) getRowByID(ctx context.Context, table string, id int) (map[string]interface{}, error) {
	pkey := dbe.tables.Table(table).Columns().GetPrimaryKey()
	row := dbe.db.QueryRowContext(ctx, fmt.Sprintf("SELECT * FROM %s WHERE %s = ?", table, pkey), id)

	columns := dbe.tables.Table(table).Columns()
	values := columns.Values()
	err := row.Scan(values...)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, &apiError{code: http.StatusNotFound, err: "record not found"}
		}
		return nil, err
	}

	return columns.ValuesToMap(values), nil
}

func (dbe *DbExplorer) createTableRow(ctx context.Context, table string, data map[string]interface{}) (map[string]interface{}, error) {
	columns := dbe.tables.Table(table).Columns()

	values, err := columns.ValuesFromMap(data, insertQuery)
	if err != nil {
		return nil, &apiError{code: http.StatusBadRequest, err: err.Error()}
	}

	placeholders := strings.Repeat("?, ", len(columns))
	placeholders = placeholders[:len(placeholders)-2]

	res, err := dbe.db.ExecContext(ctx, fmt.Sprintf("INSERT INTO %s VALUES (%s)", table, placeholders), values...)
	if err != nil {
		if v, ok := err.(*mysql.MySQLError); ok {
			return nil, &apiError{code: http.StatusBadRequest, err: v.Message}
		}

		return nil, err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}

	pkey := dbe.tables.Table(table).Columns().GetPrimaryKey()
	return map[string]interface{}{
		pkey: id,
	}, nil
}

func (dbe *DbExplorer) updateTableRowByID(ctx context.Context, table string, id int, data map[string]interface{}) (int64, error) {
	columns := dbe.tables.Table(table).Columns()

	values, err := columns.ValuesFromMap(data, updateQuery)
	if err != nil {
		return 0, &apiError{code: http.StatusBadRequest, err: err.Error()}
	}
	values = append(values, id)

	pkey := columns.GetPrimaryKey()
	placeholders := columns.PlaceholdersFromMap(data)
	res, err := dbe.db.ExecContext(ctx, fmt.Sprintf("UPDATE %s SET %s WHERE %s = ?", table, placeholders, pkey), values...)
	if err != nil {
		return 0, err
	}

	affectedID, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}

	return affectedID, nil
}

func (dbe *DbExplorer) deleteRowByID(ctx context.Context, table string, id int) (int64, error) {
	pkey := dbe.tables.Table(table).Columns().GetPrimaryKey()
	res, err := dbe.db.ExecContext(ctx, fmt.Sprintf("DELETE FROM %s WHERE %s = ?", table, pkey), id)
	if err != nil {
		return 0, err
	}

	affectedID, err := res.RowsAffected()
	if err != nil {
		return 0, err
	}

	return affectedID, nil
}
