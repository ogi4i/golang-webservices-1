package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
)

// сюда писать код
func calculateSingleHash(data string, out chan interface{}, mu *sync.Mutex) {
	crcData := make(chan string, 1)
	go func() {
		crcData <- DataSignerCrc32(data)
	}()

	var md5Data string
	md5Done := make(chan bool)
	go func() {
		mu.Lock()
		md5Data = DataSignerMd5(data)
		mu.Unlock()
		fmt.Printf("SingleHash : data=%s; md5(data)=%s\n", data, md5Data)
		md5Done <- true
	}()

	crcMd5Data := make(chan string, 1)
	go func() {
		<-md5Done
		crcMd5Data <- DataSignerCrc32(md5Data)
	}()

	crc32Data := <-crcData
	fmt.Printf("SingleHash : data=%s; crc32(data)=%s\n", data, crc32Data)
	crc32Md5Data := <-crcMd5Data
	fmt.Printf("SingleHash : data=%s; md5(data)=%s; crc32(md5(data))=%s\n", data, md5Data, crc32Md5Data)
	result := crc32Data + "~" + crc32Md5Data
	fmt.Printf("SingleHash : data=%s; crc32(data)=%s; crc32(md5(data))=%s; result=%s\n", data, crc32Data, crc32Md5Data, result)

	out <- result
}

func calculateMultiHash(data string, out chan interface{}) {
	type MultiHashResult struct {
		th     int
		result string
	}

	wgTh := new(sync.WaitGroup)
	results := make(chan *MultiHashResult, 6)
	for i := 0; i < 6; i++ {
		wgTh.Add(1)
		go func(th int, data string) {
			defer wgTh.Done()
			result := DataSignerCrc32(fmt.Sprintf("%d%s", th, data))
			results <- &MultiHashResult{
				th:     th,
				result: result,
			}
			fmt.Printf("MultiHash : data=%s; th=%d; result=%s\n", data, th, result)
		}(i, data)
	}
	wgTh.Wait()
	close(results)

	sorted := make([]*MultiHashResult, 0, 6)
	for r := range results {
		sorted = append(sorted, r)
	}

	sort.Slice(sorted, func(i, j int) bool {
		return sorted[i].th < sorted[j].th
	})

	var result string
	for _, s := range sorted {
		result += s.result
	}
	fmt.Printf("MultiHash : data=%s; result=%s\n", data, result)

	out <- result
}

func extractData(v interface{}) string {
	var data string
	switch v.(type) {
	case int:
		data = strconv.Itoa(v.(int))
	case string:
		data = v.(string)
	}

	return data
}

func SingleHash(in, out chan interface{}) {
	wgJob := new(sync.WaitGroup)
	md5Mutex := new(sync.Mutex)
	for raw := range in {
		data := extractData(raw)

		wgJob.Add(1)
		go func(data string) {
			defer wgJob.Done()
			calculateSingleHash(data, out, md5Mutex)
		}(data)
	}
	wgJob.Wait()
}

func MultiHash(in, out chan interface{}) {
	wgJob := new(sync.WaitGroup)
	for raw := range in {
		data := extractData(raw)

		wgJob.Add(1)
		go func(data string) {
			defer wgJob.Done()
			calculateMultiHash(data, out)
		}(data)
	}
	wgJob.Wait()
}

func CombineResults(in, out chan interface{}) {
	inputs := make([]string, 0)
	for raw := range in {
		inputs = append(inputs, extractData(raw))
	}

	sort.Strings(inputs)
	result := strings.Join(inputs, "_")
	fmt.Printf("CombineResults : result=%s\n", result)

	out <- result
}

func ExecutePipeline(jobs ...job) {
	wgPipeline := new(sync.WaitGroup)
	in := make(chan interface{})
	for _, j := range jobs {
		out := make(chan interface{})

		wgPipeline.Add(1)
		go func(j func(in, out chan interface{}), in, out chan interface{}) {
			defer wgPipeline.Done()
			j(in, out)
			close(out)
		}(j, in, out)

		in = out
	}
	wgPipeline.Wait()
}
