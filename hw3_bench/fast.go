package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"
)

// вам надо написать более быструю оптимальную этой функции
func FastSearch(out io.Writer) {
	type User struct {
		Name     string   `json:"name"`
		Email    string   `json:"email"`
		Browsers []string `json:"browsers"`
	}

	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}

	reader := bufio.NewScanner(file)

	user := new(User)
	var id int
	var uniqueBrowsers int
	seenBrowsers := make(map[string]interface{})

	out.Write([]byte("found users:\n"))
	for reader.Scan() {
		line := reader.Bytes()
		if !bytes.Contains(line, []byte("Android")) && !bytes.Contains(line, []byte("MSIE")) {
			id++
			continue
		}

		err = json.Unmarshal(line, user)
		if err != nil {
			panic(err)
		}

		var isAndroid bool
		var isMSIE bool
		var seenBefore bool
		for _, browser := range user.Browsers {
			if strings.Contains(browser, "Android") {
				isAndroid = true

				_, seenBefore = seenBrowsers[browser]
				if !seenBefore {
					seenBrowsers[browser] = nil
					uniqueBrowsers++
				}
			}
			if strings.Contains(browser, "MSIE") {
				isMSIE = true

				_, seenBefore = seenBrowsers[browser]
				if !seenBefore {
					seenBrowsers[browser] = nil
					uniqueBrowsers++
				}
			}
		}

		if !(isAndroid && isMSIE) {
			id++
			continue
		}

		fmt.Fprintf(out, "[%d] %s <%s>\n", id, user.Name, strings.Replace(user.Email, "@", " [at] ", -1))
		id++
	}
	fmt.Fprintf(out, "\nTotal unique browsers %d\n", uniqueBrowsers)
}
