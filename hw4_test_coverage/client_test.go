package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"sort"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// код писать тут
const (
	USERSDATAFILE = "dataset.xml"
)

var (
	EnsureTimeout = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(2 * time.Second)
	}))

	EnsureInternalServerError = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		searchServerError(w, http.StatusInternalServerError, "SearchServer fatal error")
	}))

	EnsureInvalidBody = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("123"))
	}))

	EnsureInvalidErrorBody = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("123"))
	}))

	DefaultRequest = &SearchRequest{
		Limit:      10,
		Offset:     0,
		Query:      "",
		OrderBy:    OrderByAsIs,
		OrderField: "",
	}

	QueryByAboutSearchOrderIDAscResponse = &SearchResponse{
		Users: []User{
			{Id: 0, Name: "Boyd Wolf", Age: 22, About: "Nulla cillum enim voluptate consequat laborum esse excepteur occaecat commodo nostrud excepteur ut cupidatat. Occaecat minim incididunt ut proident ad sint nostrud ad laborum sint pariatur. Ut nulla commodo dolore officia. Consequat anim eiusmod amet commodo eiusmod deserunt culpa. Ea sit dolore nostrud cillum proident nisi mollit est Lorem pariatur. Lorem aute officia deserunt dolor nisi aliqua consequat nulla nostrud ipsum irure id deserunt dolore. Minim reprehenderit nulla exercitation labore ipsum.\n", Gender: "male"},
			{Id: 2, Name: "Brooks Aguilar", Age: 25, About: "Velit ullamco est aliqua voluptate nisi do. Voluptate magna anim qui cillum aliqua sint veniam reprehenderit consectetur enim. Laborum dolore ut eiusmod ipsum ad anim est do tempor culpa ad do tempor. Nulla id aliqua dolore dolore adipisicing.\n", Gender: "male"},
			{Id: 19, Name: "Bell Bauer", Age: 26, About: "Nulla voluptate nostrud nostrud do ut tempor et quis non aliqua cillum in duis. Sit ipsum sit ut non proident exercitation. Quis consequat laboris deserunt adipisicing eiusmod non cillum magna.\n", Gender: "male"},
			{Id: 21, Name: "Johns Whitney", Age: 26, About: "Elit sunt exercitation incididunt est ea quis do ad magna. Commodo laboris nisi aliqua eu incididunt eu irure. Labore ullamco quis deserunt non cupidatat sint aute in incididunt deserunt elit velit. Duis est mollit veniam aliquip. Nulla sunt veniam anim et sint dolore.\n", Gender: "male"},
		},
		NextPage: false,
	}

	QueryByAboutSearchOrderAgeDescResponse = &SearchResponse{
		Users: []User{
			{Id: 21, Name: "Johns Whitney", Age: 26, About: "Elit sunt exercitation incididunt est ea quis do ad magna. Commodo laboris nisi aliqua eu incididunt eu irure. Labore ullamco quis deserunt non cupidatat sint aute in incididunt deserunt elit velit. Duis est mollit veniam aliquip. Nulla sunt veniam anim et sint dolore.\n", Gender: "male"},
			{Id: 19, Name: "Bell Bauer", Age: 26, About: "Nulla voluptate nostrud nostrud do ut tempor et quis non aliqua cillum in duis. Sit ipsum sit ut non proident exercitation. Quis consequat laboris deserunt adipisicing eiusmod non cillum magna.\n", Gender: "male"},
			{Id: 2, Name: "Brooks Aguilar", Age: 25, About: "Velit ullamco est aliqua voluptate nisi do. Voluptate magna anim qui cillum aliqua sint veniam reprehenderit consectetur enim. Laborum dolore ut eiusmod ipsum ad anim est do tempor culpa ad do tempor. Nulla id aliqua dolore dolore adipisicing.\n", Gender: "male"},
			{Id: 0, Name: "Boyd Wolf", Age: 22, About: "Nulla cillum enim voluptate consequat laborum esse excepteur occaecat commodo nostrud excepteur ut cupidatat. Occaecat minim incididunt ut proident ad sint nostrud ad laborum sint pariatur. Ut nulla commodo dolore officia. Consequat anim eiusmod amet commodo eiusmod deserunt culpa. Ea sit dolore nostrud cillum proident nisi mollit est Lorem pariatur. Lorem aute officia deserunt dolor nisi aliqua consequat nulla nostrud ipsum irure id deserunt dolore. Minim reprehenderit nulla exercitation labore ipsum.\n", Gender: "male"},
		},
		NextPage: false,
	}

	QueryByAboutLimitSearchResponse = &SearchResponse{
		Users: []User{
			{Id: 0, Name: "Boyd Wolf", Age: 22, About: "Nulla cillum enim voluptate consequat laborum esse excepteur occaecat commodo nostrud excepteur ut cupidatat. Occaecat minim incididunt ut proident ad sint nostrud ad laborum sint pariatur. Ut nulla commodo dolore officia. Consequat anim eiusmod amet commodo eiusmod deserunt culpa. Ea sit dolore nostrud cillum proident nisi mollit est Lorem pariatur. Lorem aute officia deserunt dolor nisi aliqua consequat nulla nostrud ipsum irure id deserunt dolore. Minim reprehenderit nulla exercitation labore ipsum.\n", Gender: "male"},
		},
		NextPage: true,
	}

	QueryByNameSearchResponse = &SearchResponse{
		Users: []User{
			{Id: 0, Name: "Boyd Wolf", Age: 22, About: "Nulla cillum enim voluptate consequat laborum esse excepteur occaecat commodo nostrud excepteur ut cupidatat. Occaecat minim incididunt ut proident ad sint nostrud ad laborum sint pariatur. Ut nulla commodo dolore officia. Consequat anim eiusmod amet commodo eiusmod deserunt culpa. Ea sit dolore nostrud cillum proident nisi mollit est Lorem pariatur. Lorem aute officia deserunt dolor nisi aliqua consequat nulla nostrud ipsum irure id deserunt dolore. Minim reprehenderit nulla exercitation labore ipsum.\n", Gender: "male"},
		},
		NextPage: false,
	}
)

type (
	UsersXML struct {
		XMLName xml.Name   `xml:"root" json:"-"`
		Users   []*UserXML `xml:"row"`
	}

	UserXML struct {
		XMLName   xml.Name `xml:"row"`
		ID        int      `xml:"id"`
		FirstName string   `xml:"first_name"`
		LastName  string   `xml:"last_name"`
		Age       int      `xml:"age"`
		About     string   `xml:"about"`
		Gender    string   `xml:"gender"`
	}

	TestCase struct {
		Client           *SearchClient
		Server           *httptest.Server
		Request          *SearchRequest
		ExpectedResponse *SearchResponse
	}
)

func (s *UsersXML) convertToUser() []*User {
	users := make([]*User, 0, len(s.Users))
	for _, user := range s.Users {
		users = append(users, &User{
			Id:     user.ID,
			Name:   fmt.Sprintf("%s %s", user.FirstName, user.LastName),
			Age:    user.Age,
			About:  user.About,
			Gender: user.Gender,
		})
	}

	return users
}

func (s *UsersXML) filterByQuery(query string) {
	users := make([]*UserXML, 0, len(s.Users))
	for _, u := range s.Users {
		if strings.Contains(u.About, query) || strings.Contains(fmt.Sprintf("%s %s", u.FirstName, u.LastName), query) {
			users = append(users, u)
		}
	}
	s.Users = users
}

func convertToInt(s string) (res int, err error) {
	if s != "" {
		res, err = strconv.Atoi(s)
	}
	return
}

func searchServerError(w http.ResponseWriter, code int, error string) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	body, _ := json.Marshal(&SearchErrorResponse{Error: error})
	w.WriteHeader(code)
	w.Write(body)
}

func SearchServer(token string) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		accessToken := r.Header.Get("AccessToken")
		if accessToken != token {
			searchServerError(w, http.StatusUnauthorized, "Bad AccessToken")
			return
		}

		var body []byte

		limit, err := convertToInt(r.FormValue("limit"))
		if err != nil {
			searchServerError(w, http.StatusBadRequest, "ErrorInvalidLimit")
			return
		}

		offset, err := convertToInt(r.FormValue("offset"))
		if err != nil {
			searchServerError(w, http.StatusBadRequest, "ErrorInvalidOffset")
			return
		}

		orderBy, err := convertToInt(r.FormValue("order_by"))
		if err != nil {
			searchServerError(w, http.StatusBadRequest, "ErrorInvalidOrderBy")
			return
		}

		reqData := &SearchRequest{
			Limit:      limit,
			Offset:     offset,
			Query:      r.FormValue("query"),
			OrderField: r.FormValue("order_field"),
			OrderBy:    orderBy,
		}

		if reqData.OrderField == "" {
			reqData.OrderField = "Name"
		}

		if orderBy < -1 || orderBy > 1 {
			searchServerError(w, http.StatusBadRequest, "ErrorBadOrderBy")
			return
		}

		if reqData.OrderField != "Id" && reqData.OrderField != "Name" && reqData.OrderField != "Age" {
			searchServerError(w, http.StatusBadRequest, "ErrorBadOrderField")
			return
		}

		xmlData, err := ioutil.ReadFile(USERSDATAFILE)
		if err != nil {
			searchServerError(w, http.StatusInternalServerError, "SearchServer fatal error")
			return
		}

		usersXML := new(UsersXML)
		err = xml.Unmarshal(xmlData, &usersXML)
		if err != nil {
			searchServerError(w, http.StatusInternalServerError, "SearchServer fatal error")
			return
		}

		if reqData.Query != "" {
			usersXML.filterByQuery(reqData.Query)
		}

		users := usersXML.convertToUser()

		if reqData.OrderBy != OrderByAsIs {
			sort.Slice(users, func(i, j int) bool {
				switch reqData.OrderField {
				case "Name":
					return (users[i].Name < users[j].Name) != (reqData.OrderBy == OrderByDesc)
				case "Id":
					return (users[i].Id < users[j].Id) != (reqData.OrderBy == OrderByDesc)
				case "Age":
					return (users[i].Age < users[j].Age) != (reqData.OrderBy == OrderByDesc)
				}
				return false
			})
		}

		if limit >= len(users) {
			body, err = json.Marshal(users[offset:])
		} else {
			body, err = json.Marshal(users[offset:limit])
		}
		if err != nil {
			searchServerError(w, http.StatusInternalServerError, "SearchServer fatal error")
			return
		}

		w.Write(body)
	}))
}

func setupTestCase(accessToken string, server *httptest.Server) *TestCase {
	client := &SearchClient{
		AccessToken: accessToken,
		URL:         server.URL,
	}

	return &TestCase{
		Client: client,
		Server: server,
	}
}

func TestSearchClientFindUsers(t *testing.T) {
	tc := setupTestCase("1234", SearchServer("1234"))
	defer tc.Server.Close()
	tc.Request = DefaultRequest

	t.Run("negative limit", func(t *testing.T) {
		tc.Request.Limit = -1

		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Empty(t, resp)
		assert.EqualError(t, err, "limit must be > 0")
	})

	tc.Request.Limit = 10

	t.Run("negative offset", func(t *testing.T) {
		tc.Request.Offset = -1

		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Empty(t, resp)
		assert.EqualError(t, err, "offset must be > 0")
	})

	tc.Request.Offset = 0

	t.Run("invalid OrderFeld", func(t *testing.T) {
		tc.Request.OrderField = "123"

		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Empty(t, resp)
		assert.EqualError(t, err, "OrderFeld 123 invalid")
	})

	tc.Request.OrderField = ""

	t.Run("invalid order by", func(t *testing.T) {
		tc.Request.OrderBy = 2

		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Empty(t, resp)
		assert.EqualError(t, err, "unknown bad request error: ErrorBadOrderBy")
	})

	tc.Request.OrderBy = OrderByAsIs
	tc.Request.Limit = 50

	t.Run("query by name", func(t *testing.T) {
		tc.Request.Query = "Boyd Wolf"

		tc.ExpectedResponse = QueryByNameSearchResponse
		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Equal(t, tc.ExpectedResponse, resp)
		assert.NoError(t, err)
	})

	t.Run("query by about with next page", func(t *testing.T) {
		tc.Request.Limit = 1
		tc.Request.Query = "Occaecat"

		tc.ExpectedResponse = QueryByAboutLimitSearchResponse
		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Equal(t, tc.ExpectedResponse, resp)
		assert.NoError(t, err)
	})

	tc.Request.Limit = 10

	t.Run("query by about order by age desc", func(t *testing.T) {
		tc.Request.OrderBy = OrderByDesc
		tc.Request.Query = "Nulla"
		tc.Request.OrderField = "Age"

		tc.ExpectedResponse = QueryByAboutSearchOrderAgeDescResponse
		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Equal(t, tc.ExpectedResponse, resp)
		assert.NoError(t, err)
	})

	t.Run("query by about order by id asc", func(t *testing.T) {
		tc.Request.OrderBy = OrderByAsc
		tc.Request.Query = "Nulla"
		tc.Request.OrderField = "Id"

		tc.ExpectedResponse = QueryByAboutSearchOrderIDAscResponse
		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Equal(t, tc.ExpectedResponse, resp)
		assert.NoError(t, err)
	})

	tc.Client.URL = ""

	t.Run("unknown net error", func(t *testing.T) {
		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Empty(t, resp)
		assert.EqualError(t, err, "unknown error Get ?limit=11&offset=0&order_by=-1&order_field=Id&query=Nulla: unsupported protocol scheme \"\"")
	})
}

func TestSearchClientFindUsersUnauthorized(t *testing.T) {
	tc := setupTestCase("4321", SearchServer("1234"))
	defer tc.Server.Close()

	t.Run("wrong AccessToken", func(t *testing.T) {
		tc.Request = DefaultRequest
		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Empty(t, resp)
		assert.EqualError(t, err, "Bad AccessToken")
	})
}

func TestSearchClientFindUsersInternalServerError(t *testing.T) {
	tc := setupTestCase("1234", EnsureInternalServerError)
	defer tc.Server.Close()
	tc.Request = DefaultRequest

	t.Run("internal server error", func(t *testing.T) {
		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Empty(t, resp)
		assert.EqualError(t, err, "SearchServer fatal error")
	})
}

func TestSearchClientFindUsersTimeout(t *testing.T) {
	tc := setupTestCase("1234", EnsureTimeout)
	defer tc.Server.Close()
	tc.Request = DefaultRequest

	t.Run("timeout", func(t *testing.T) {
		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Empty(t, resp)
		assert.EqualError(t, err, "timeout for limit=11&offset=0&order_by=-1&order_field=Id&query=Nulla")
	})
}

func TestSearchClientFindUsersInvalidBody(t *testing.T) {
	tc := setupTestCase("1234", EnsureInvalidBody)
	defer tc.Server.Close()
	tc.Request = DefaultRequest

	t.Run("invalid response body", func(t *testing.T) {
		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Empty(t, resp)
		assert.EqualError(t, err, "cant unpack result json: json: cannot unmarshal number into Go value of type []main.User")
	})
}

func TestSearchClientFindUsersInvalidErrorBody(t *testing.T) {
	tc := setupTestCase("1234", EnsureInvalidErrorBody)
	defer tc.Server.Close()
	tc.Request = DefaultRequest

	t.Run("invalid error response body", func(t *testing.T) {
		resp, err := tc.Client.FindUsers(*tc.Request)
		assert.Empty(t, resp)
		assert.EqualError(t, err, "cant unpack error json: json: cannot unmarshal number into Go value of type main.SearchErrorResponse")
	})
}
